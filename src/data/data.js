const data = {
    properties: [
        {
            "id": "11250",
            "index": 0,
            "picture": "https://www.w3schools.com/howto/img_nature_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11251",
            "index": 1,
            "picture": "https://www.w3schools.com/howto/img_snow_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11252",
            "index": 2,
            "picture": "https://www.w3schools.com/howto/img_nature_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11253",
            "index": 3,
            "picture": "https://www.w3schools.com/howto/img_woods_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11254",
            "index": 4,
            "picture": "https://www.w3schools.com/howto/img_5terre_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11255",
            "index": 5,
            "picture": "https://www.w3schools.com/howto/img_lights_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11256",
            "index": 6,
            "picture": "https://www.w3schools.com/howto/img_nature_wide.jpg",
            "info": "Image"
        },
        {
            "id": "11257",
            "index": 7,
            "picture": "https://www.w3schools.com/howto/img_snow_wide.jpg",
            "info": "Image"
        }
    ]
}

export default data;