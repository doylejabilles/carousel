import React from 'react';

const Card = (property) => {
    const { index, picture, info  } = property.property;
    return(
        <div id={`card-${index}`} className='card'
        onClick = { () => { property.currentIndex(index) } }
        >
            <img src={picture} alt={info}/>
            <div className="info">
                {info}<br/>
            </div>
        </div>
    )

}

export default Card;