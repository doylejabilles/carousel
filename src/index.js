import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';

const destination = document.getElementById('container');

ReactDOM.render(<App/>,destination);