import React, { Component } from 'react';
import data from './data/data'; //data source
import Card from './Card';
import { setInterval } from 'timers';
import './index.css';
import './card.scss'; //for card style and animation

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            properties: data.properties,
            property: data.properties[0],
            onSlide: true
        }
    };

    //next navigation
    nextProperty = () => {
        const newIndex = this.state.property.index+1;
        this.setState({
            property: data.properties[newIndex],
        })
    }

    //prev navigation
    prevProperty = () => {
        const newIndex = this.state.property.index-1;
        this.setState({
            property: data.properties[newIndex],
            onSlide: false
        })
    }

    //click to specific slide
    currentIndex = (idx) => {
        this.setState({
            property: data.properties[idx],
            onSlide: false
        })
    }

    render() {
        const  { properties, property } = this.state;

        if( !this.state.onSlide ) {
            setInterval( () => {
                this.setState({
                    onSlide: true
                })
            }, 3000 );
        }

        setTimeout( () => {
            if( this.state.onSlide ) {
                if( this.state.property.index !== data.properties.length-1 ) {
                    this.nextProperty();
                }
            }
        }, 5000 );

        return(
            <div className="App">  

                <button
                onClick={ () => this.prevProperty() }
                disabled={ property.index === 0 } 
                className="glyphicon glyphicon-chevron-left">
                </button>

                <button
                    onClick={ () => { 
                        this.nextProperty();
                        this.setState({
                            onSlide: false
                        })
                    } }
                    disabled={ property.index === data.properties.length-1 } 
                    className="glyphicon glyphicon-chevron-right">
                </button>

                <br/><br/>

                <div className="page">
                    <div className={`cards-slider active-slide-${property.index}`}>
                        <div className="cards-slider-wrapper" 
                        style={{ 'transform': `translateX(-${property.index*(100/properties.length)}%)`}}
                        >
                        {
                            properties.map( property => 
                            <Card key={property.id} property={property} currentIndex={ (idx) => this.currentIndex(idx) } /> )
                        }
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default App;